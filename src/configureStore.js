import { applyMiddleware, createStore } from 'redux';
import { routerMiddleware } from 'connected-react-router';
import createHistory from 'history/createBrowserHistory';
import createSagaMiddleware from 'redux-saga';
import { composeWithDevTools } from 'redux-devtools-extension';

import rootReducer from './reducers/index';
import rootSaga from './sagas/index';

let store, history;

export default () => {
  if (store !== undefined) {
    return { history, store };
  }

  history = createHistory();
  history.listen((location, action) => {
    // TODO: record to Google Analytics
  });

  const sagaMiddleware = createSagaMiddleware();
  store = createStore(
    rootReducer(history),
    composeWithDevTools(applyMiddleware(routerMiddleware(history), sagaMiddleware)),
  );

  sagaMiddleware.run(rootSaga);
  return { history, store };
};
