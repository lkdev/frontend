import React from 'react';
import { Query } from 'react-apollo';
import gql from 'graphql-tag';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';
import { Menu } from 'antd';

class RoomMenu extends React.Component {
  render() {
    return (
      <Query
        query={gql`
          query {
            allRooms {
              id
              name
              slug
            }
          }
        `}>
        {({ loading, error, data }) => {
          if (loading) return 'Loading...';
          if (error) return 'Error :(';
          return (
            <Menu
              mode="inline"
              style={{ height: '100%' }}
              selectedKeys={[this.props.pathname.replace('/', '')]}>
              {data.allRooms.map(({ name, slug }) => (
                <Menu.Item key={slug}>
                  <Link to={`/${slug}`}>{name}</Link>
                </Menu.Item>
              ))}
            </Menu>
          );
        }}
      </Query>
    );
  }
}

export default connect((state) => ({
  pathname: state.router.location.pathname,
}))(RoomMenu);
