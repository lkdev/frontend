/**
 * This config is for application state which can be adjusted during the build
 */
export default {
  features: {
    // TODO: add features flags
  },
  configs: {
    backend: "api.meetup.profiq.com",
  },
};
